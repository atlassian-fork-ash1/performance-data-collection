package com.atlassianlabs.perftools.collectors.results;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassianlabs.perftools.collectors.results.CumulativeResult.NO_RESULTS_ERROR;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CumulativeResultTest {
    @Test
    public void itShouldProvideFailureReasonsIfThereIsNoResult() {
        final List<String> failureReasons = Arrays.asList("Unable to collect with JCmdThreadDump: Connection refused",
                "Unable to collect with JStackThreadDump: Connection refused"
        );

        final CumulativeResult result = new TestingResults(failureReasons, "", "");

        assertThat(result.toString(), is(NO_RESULTS_ERROR + "\n\n" +
                "Unable to collect with JCmdThreadDump: Connection refused\n" +
                "Unable to collect with JStackThreadDump: Connection refused"));
    }

    @Test
    public void itShouldProvideResultsIfTheyExist() {
        final CumulativeResult result = new TestingResults(Collections.singletonList("meowmeow"), "JStackThreadDump", "pewpew");

        assertThat(result.toString(), is("pewpew"));
    }

    class TestingResults extends CumulativeResult {
        TestingResults(List<String> failureReasons, String generatedBy, String result) {
            super(failureReasons, generatedBy, result);
        }
    }
}