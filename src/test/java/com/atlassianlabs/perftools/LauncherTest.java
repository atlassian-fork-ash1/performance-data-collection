package com.atlassianlabs.perftools;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.TestRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LauncherTest {
    private static final String JAVA_VERSION = "java.specification.version";

    @Rule
    public final TestRule restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void itShouldNotBeJava8BecauseItsJava9() {
        System.setProperty(JAVA_VERSION, "9");
        assertFalse(Launcher.isJava8());
    }

    @Test
    public void itShouldBeJava8() {
        System.setProperty(JAVA_VERSION, "1.8");
        assertTrue(Launcher.isJava8());
    }

    @Test
    public void itShouldBeJava8IfWeHaveALongJavaVersion() {
        System.setProperty(JAVA_VERSION, "1.84-SNAPSHOT");
        assertTrue(Launcher.isJava8());
    }
}