package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.collectors.cpu.CpuUsageCollector;
import com.atlassianlabs.perftools.collectors.cpu.MBeanCpuUsage;
import com.atlassianlabs.perftools.collectors.cpu.TopCpuUsage;
import com.atlassianlabs.perftools.collectors.results.CumulativeResult;
import com.atlassianlabs.perftools.rest.exceptions.NotFoundException;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.Map;

import static com.atlassianlabs.perftools.helper.MapHelper.asMap;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CpuUsageServiceTest extends AbstractBaseTest {
    private static final long PID = 4242L;
    private static final String TOP_FAILURE_REASON = "could not locate neocentral matter converter";
    private static final String MBEAN_FAILURE_REASON = "how do you flip an egg?";
    private static final String MBEAN_CPU_USAGE = "MBeanCpuUsage";
    private static final String TOP_CPU_USAGE = "TopCpuUsage";

    @Mock
    private AnalyticsService analyticsService;
    @Mock
    private ApplicationContext context;
    @Mock
    private TopCpuUsage topCpuUsage;
    @Mock
    private MBeanCpuUsage mBeanCpuUsage;

    @InjectMocks
    private CpuUsageService service;

    @Before
    public void setUp() throws Exception {
        super.setUp();

        when(context.getBeansOfType(CpuUsageCollector.class)).thenReturn(asMap(
                MBEAN_CPU_USAGE, mBeanCpuUsage,
                TOP_CPU_USAGE, topCpuUsage
        ));

        // Use the actual priority here rather than mocking it, cause we want to test the real thing.
        when(topCpuUsage.getPriority()).thenCallRealMethod();
        when(topCpuUsage.collectFromProcess(PID)).thenReturn(CPU_RESULT_STRING);
        when(topCpuUsage.getName()).thenReturn(TOP_CPU_USAGE);
        when(mBeanCpuUsage.getPriority()).thenCallRealMethod();
        when(mBeanCpuUsage.collectFromProcess(PID)).thenReturn(CPU_RESULT_STRING);
        when(mBeanCpuUsage.getName()).thenReturn(MBEAN_CPU_USAGE);
    }

    @Test
    public void doesGetThreadDumpGeneratorsUsePriority() {
        final Map<String, CpuUsageCollector> collectors = service.getCollectors();

        assertThat(asList(collectors.values().toArray()), is(asList(topCpuUsage, mBeanCpuUsage)));
    }

    @Test
    public void ifFirstGeneratorFailsDoWeContinue() throws Exception {
        when(topCpuUsage.collectFromProcess(PID)).thenThrow(new IOException("nope"));

        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getResult(), equalTo(successfulCpuUsageResult.getResult()));
        verify(topCpuUsage).collectFromProcess(PID);
        verify(mBeanCpuUsage).collectFromProcess(PID);
    }

    @Test
    public void ifFirstGeneratorSucceedsDoWeStop() throws Exception {
        service.collectFromPid(PID);

        verify(topCpuUsage).collectFromProcess(PID);
        verify(mBeanCpuUsage, times(0)).collectFromProcess(PID);
    }

    @Test
    public void ifBothGeneratorsFailDoWeSeeFailureReasonsAndNoUsage() throws Exception {
        when(mBeanCpuUsage.collectFromProcess(PID)).thenThrow(new IOException(TOP_FAILURE_REASON));
        when(topCpuUsage.collectFromProcess(PID)).thenThrow(new IOException(MBEAN_FAILURE_REASON));

        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getResult(), isEmptyOrNullString());
        assertThat(result.getFailureReasons(), hasSize(2));
        assertThat(result.getFailureReasons().get(0), containsString(MBEAN_FAILURE_REASON));
        assertThat(result.getFailureReasons().get(1), containsString(TOP_FAILURE_REASON));
    }

    @Test
    public void ifOneGeneratorFailsDoWeSeeFailureReasonAndUsage() throws Exception {
        when(topCpuUsage.collectFromProcess(PID)).thenThrow(new IOException(TOP_FAILURE_REASON));

        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getResult(), is(CPU_RESULT_STRING));
        assertThat(result.getFailureReasons(), hasSize(1));
        assertThat(result.getFailureReasons().get(0), containsString(TOP_FAILURE_REASON));
    }

    @Test(expected = NotFoundException.class)
    public void ifGeneratorIdIsOutOfBoundsDoWeRethrowException() throws Exception {
        service.collectFromPid(PID, "PotatoBake");
    }

    @Test
    public void ifWeSpecifyGeneratorIdDoesItOnlyUseThatOne() throws Exception {
        service.collectFromPid(PID, MBEAN_CPU_USAGE);

        verify(topCpuUsage, times(0)).collectFromProcess(PID);
        verify(mBeanCpuUsage).collectFromProcess(PID);
    }

    @Test
    public void doesGeneratedByGetSet() {
        final CumulativeResult result = service.collectFromPid(PID);

        assertThat(result.getGeneratedBy(), is(topCpuUsage.getName()));
    }

    @Test
    public void itShouldSendAnalyticsOnSuccess() {
        service.collectFromPid(PID);

        verifyAnalytics(TOP_CPU_USAGE, "");
    }

    @Test
    public void itShouldSendAnalyticsOnOneSuccessAndOneFailed() throws Exception {
        when(topCpuUsage.collectFromProcess(PID)).thenThrow(new IOException("nope"));

        service.collectFromPid(PID);

        verifyAnalytics(MBEAN_CPU_USAGE, TOP_CPU_USAGE);
    }

    @Test
    public void itShouldSendAnalyticsIfBothFailed() throws Exception {
        when(mBeanCpuUsage.collectFromProcess(PID)).thenThrow(new IOException(TOP_FAILURE_REASON));
        when(topCpuUsage.collectFromProcess(PID)).thenThrow(new IOException(MBEAN_FAILURE_REASON));

        service.collectFromPid(PID);

        verifyAnalytics("N/A", "TopCpuUsage,MBeanCpuUsage");
    }

    private void verifyAnalytics(String collectorName, String failedCollectors) {
        final ArgumentCaptor<Map<String, String>> captor = ArgumentCaptor.forClass(Map.class);
        verify(analyticsService).sendAsyncEvent(eq("pdc.server.collect.CpuUsageCollector"), captor.capture());
        final Map<String, String> eventProperties = captor.getValue();
        MatcherAssert.assertThat(eventProperties, hasEntry("collector.name", collectorName));
        MatcherAssert.assertThat(eventProperties, hasEntry("failed.collectors", failedCollectors));
    }
}
