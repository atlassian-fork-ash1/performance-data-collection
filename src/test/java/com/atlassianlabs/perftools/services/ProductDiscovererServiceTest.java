package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.AbstractBaseTest;
import com.atlassianlabs.perftools.PublicJavaProcessId;
import com.atlassianlabs.perftools.product.DescriptorConverter;
import com.atlassianlabs.perftools.product.DescriptorConverterFactory;
import com.atlassianlabs.perftools.product.Product;
import com.atlassianlabs.perftools.product.descriptors.ConfluenceDescriptor;
import com.atlassianlabs.perftools.product.descriptors.FisheyeDescriptor;
import com.atlassianlabs.perftools.product.descriptors.JiraDescriptor;
import com.atlassianlabs.perftools.product.descriptors.ProductDescriptor;
import org.gridkit.lab.jvm.attach.JavaProcessDetails;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import static com.atlassianlabs.perftools.helper.MapHelper.asMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

// There are so many bloody mocks in this because of all the different products.
@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductDiscovererServiceTest extends AbstractBaseTest {
    @Mock
    private ApplicationContext context;
    @Mock
    private DescriptorConverterFactory converterFactory;
    @Mock
    private VirtualMachineService virtualMachineService;
    @Mock
    private AnalyticsService analyticsService;

    @Mock
    private PublicJavaProcessId jiraJavaProcessId;
    @Mock
    private PublicJavaProcessId confJavaProcessId;
    @Mock
    private PublicJavaProcessId fishEyeJavaProcessId;
    @Mock
    private PublicJavaProcessId unknownJavaProcessId;

    @Mock
    private JavaProcessDetails jiraJavaProcessDetails;
    @Mock
    private JavaProcessDetails confJavaProcessDetails;
    @Mock
    private JavaProcessDetails fishEyeJavaProcessDetails;
    @Mock
    private JavaProcessDetails unknownJavaProcessDetails;

    @Mock
    private DescriptorConverter jiraConverter;
    @Mock
    private DescriptorConverter confConverter;
    @Mock
    private DescriptorConverter fishEyeConverter;
    @Mock
    private DescriptorConverter unknownConverter;

    @Mock
    private JiraDescriptor jiraDescriptor;
    @Mock
    private FisheyeDescriptor fishEyeDescriptor;
    @Mock
    private ConfluenceDescriptor confDescriptor;

    @InjectMocks
    private ProductDiscovererService service;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        when(context.getBeansOfType(ProductDescriptor.class)).thenReturn(asMap(
                "jira", jiraDescriptor,
                "fecru", fishEyeDescriptor,
                "carnie", confDescriptor
        ));
        when(converterFactory.getDescriptor(jiraJavaProcessDetails)).thenReturn(jiraConverter);
        when(converterFactory.getDescriptor(confJavaProcessDetails)).thenReturn(confConverter);
        when(converterFactory.getDescriptor(fishEyeJavaProcessDetails)).thenReturn(fishEyeConverter);
        when(converterFactory.getDescriptor(unknownJavaProcessDetails)).thenReturn(unknownConverter);

        when(jiraConverter.apply(any(ProductDescriptor.class))).thenReturn(Optional.empty());
        when(confConverter.apply(any(ProductDescriptor.class))).thenReturn(Optional.empty());
        when(fishEyeConverter.apply(any(ProductDescriptor.class))).thenReturn(Optional.empty());
        when(unknownConverter.apply(any(ProductDescriptor.class))).thenReturn(Optional.empty());

        when(virtualMachineService.listRunningVirtualMachines()).thenReturn(
                Arrays.asList(jiraJavaProcessId, confJavaProcessId, unknownJavaProcessId, fishEyeJavaProcessId));

        when(virtualMachineService.getProcessDetails(JIRA_PID)).thenReturn(jiraJavaProcessDetails);
        when(virtualMachineService.getProcessDetails(CONF_PID)).thenReturn(confJavaProcessDetails);
        when(virtualMachineService.getProcessDetails(FISH_PID)).thenReturn(fishEyeJavaProcessDetails);
        when(virtualMachineService.getProcessDetails(UNKNOWN_PID)).thenReturn(unknownJavaProcessDetails);

        when(jiraJavaProcessId.getPID()).thenReturn(JIRA_PID);
        when(confJavaProcessId.getPID()).thenReturn(CONF_PID);
        when(fishEyeJavaProcessId.getPID()).thenReturn(FISH_PID);
        when(unknownJavaProcessId.getPID()).thenReturn(UNKNOWN_PID);
    }

    @Test
    public void doesGetProductReturnConfluence() {
        when(confConverter.apply(confDescriptor)).thenReturn(Optional.of(confProduct));

        final Optional<Product> results = service.getProduct(confJavaProcessDetails);

        assertThat("There are no results present and should be!", results.isPresent(), is(true));
        assertThat(results.get().getName(), is(confProduct.getName()));
    }

    @Test
    public void doesGetProductReturnJira() {
        when(jiraConverter.apply(jiraDescriptor)).thenReturn(Optional.of(jiraProduct));

        final Optional<Product> results = service.getProduct(jiraJavaProcessDetails);

        assertThat("There are no results present and should be!", results.isPresent(), is(true));
        assertThat(results.get().getName(), is(jiraProduct.getName()));
    }

    @Test
    public void doesGetProductReturnEmpty() {
        final Optional<Product> results = service.getProduct(unknownJavaProcessDetails);

        assertThat("There are results present and should not be!", results.isPresent(), is(false));
    }

    @Test
    public void doesGetProductByIdReturnFishEye() {
        when(fishEyeConverter.apply(fishEyeDescriptor)).thenReturn(Optional.of(fishEyeProduct));
        when(virtualMachineService.getProcessDetails(FISH_PID)).thenReturn(fishEyeJavaProcessDetails);

        final Optional<Product> results = service.getProduct(FISH_PID);

        assertThat("There are no results present and should be!", results.isPresent(), is(true));
        assertThat(results.get().getName(), is(fishEyeProduct.getName()));
    }

    @Test
    public void doesGetAllReturnAllTheThings() {
        when(jiraConverter.apply(jiraDescriptor)).thenReturn(Optional.of(jiraProduct));
        when(fishEyeConverter.apply(fishEyeDescriptor)).thenReturn(Optional.of(fishEyeProduct));
        when(confConverter.apply(confDescriptor)).thenReturn(Optional.of(confProduct));

        final List<Product> productList = service.getAllProducts();

        assertThat(productList, hasSize(3));
        assertThat(productList, hasItem(jiraProduct));
        assertThat(productList, hasItem(fishEyeProduct));
        assertThat(productList, hasItem(confProduct));
    }

    @Test(expected = NoSuchElementException.class)
    public void getProductShouldHandleNulls() {
        service.getProduct(null).get();
    }

    @Test
    public void itShouldSubmitAnalyticsOnSuccess() {
        when(confConverter.apply(confDescriptor)).thenReturn(Optional.of(confProduct));

        service.getProduct(confJavaProcessDetails);

        verifyAnalytics("Confluence");
    }

    @Test
    public void itShouldSubmitAnalyticsIfNoProductsFound() {
        service.getAllProducts();

        verifyAnalytics("None");
    }

    @Test
    public void itShouldNotSubmitAnalyticsOnFail() {
        service.getProduct(confJavaProcessDetails);

        verifyZeroInteractions(analyticsService);
    }

    private void verifyAnalytics(String productName) {
        final ArgumentCaptor<Map<String, String>> captor = ArgumentCaptor.forClass(Map.class);
        verify(analyticsService).sendAsyncEvent(eq("pdc.server.product.ProductDiscovererService"), captor.capture());
        final Map<String, String> eventProperties = captor.getValue();
        MatcherAssert.assertThat(eventProperties, hasEntry("product.name", productName));
    }
}
