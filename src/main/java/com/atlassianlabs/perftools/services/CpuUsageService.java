package com.atlassianlabs.perftools.services;

import com.atlassianlabs.perftools.collectors.cpu.CpuUsageCollector;
import com.atlassianlabs.perftools.collectors.results.CpuUsageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Component
public class CpuUsageService extends BaseService<CpuUsageCollector, CpuUsageResult> {

    @Autowired
    public CpuUsageService(@NotNull final AnalyticsService analyticsService, @NotNull final ApplicationContext context) {
        super(analyticsService, context, CpuUsageCollector.class);
    }

    @Override
    protected CpuUsageResult newResult(@NotNull List<String> failureReasons, @NotNull String generatedBy, @NotNull String result) {
        return new CpuUsageResult(failureReasons, generatedBy, result);
    }
}
