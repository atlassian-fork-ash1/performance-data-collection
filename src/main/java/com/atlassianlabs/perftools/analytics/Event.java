package com.atlassianlabs.perftools.analytics;

import com.atlassianlabs.perftools.services.AnalyticsService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.copyOf;
import static java.util.Objects.requireNonNull;

/**
 * POJO for amplitude analytics events.
 * Expected to be used with {@link AnalyticsService#sendAsyncEvent(com.atlassianlabs.perftools.analytics.Event)}.
 */
public class Event {
    @JsonProperty("user_id")
    private final String userId;
    @JsonProperty("event_type")
    private final String eventType;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("event_properties")
    private final Map<String, String> eventProperties;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("user_properties")
    private final Map<String, String> userProperties;
    @JsonProperty
    private final Long time;

    /**
     * @param userId          ID that specifies what the user is.
     * @param eventType       Unique identifier for the event.
     * @param eventProperties Key-value pairs that represent additional data to be sent along with the event.
     * @param userProperties  Key-value pairs that represent additional data tied to the user.
     * @param time            Ms time from epoch.
     */
    public Event(@NotNull String userId, @NotNull String eventType,
                 @NotNull Map<String, String> eventProperties, @NotNull Map<String, String> userProperties,
                 @NotNull Long time) {
        this.userId = requireNonNull(userId);
        this.eventType = requireNonNull(eventType);
        this.eventProperties = copyOf(requireNonNull(eventProperties));
        this.userProperties = copyOf(requireNonNull(userProperties));
        this.time = requireNonNull(time);
    }

    public String getUserId() {
        return userId;
    }

    public String getEventType() {
        return eventType;
    }

    public Map<String, String> getEventProperties() {
        return eventProperties;
    }

    public Map<String, String> getUserProperties() {
        return userProperties;
    }

    public Long getTime() {
        return time;
    }
}
