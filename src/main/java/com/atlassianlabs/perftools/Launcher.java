package com.atlassianlabs.perftools;

import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.tools.ToolProvider;

/**
 * Performance Data Collection Tools
 * <br>
 * Used for collection performance data from Atlassian applications, to be used for diagnosing and troubleshooting problems.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
public class Launcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);
    private static final Logger CONSOLE = LoggerFactory.getLogger("console");

    public static void main(String args[]) {
        LOGGER.debug("-------------------------------");
        LOGGER.debug("Starting perf collection tools.");
        LOGGER.debug("-------------------------------");

        LOGGER.debug("Using Java from {}", System.getProperty("java.home", "Unknown"));

        if (isJRE() || !isJava8()) {
            CONSOLE.info("Java Development Kit (JDK) 8 is required to use this tool, please install and/or set it up so the tool runs using it.");
            System.exit(1);
        }

        new SpringApplicationBuilder(Launcher.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }

    private static boolean isJRE() {
        return ToolProvider.getSystemJavaCompiler() == null;
    }

    @VisibleForTesting
    static boolean isJava8() {
        return System.getProperty("java.specification.version").startsWith("1.8");
    }
}
