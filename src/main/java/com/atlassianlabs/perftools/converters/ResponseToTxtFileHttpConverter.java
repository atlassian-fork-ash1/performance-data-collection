package com.atlassianlabs.perftools.converters;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.nio.charset.Charset.defaultCharset;
import static java.time.LocalDateTime.now;
import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;


/**
 * This takes an object, calls toString() on it and then returns that result as a txt file.
 */
public class ResponseToTxtFileHttpConverter extends AbstractHttpMessageConverter {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmm");

    @Override
    protected boolean supports(Class clazz) {
        return true;
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return singletonList(APPLICATION_OCTET_STREAM);
    }

    @Override
    protected void writeInternal(Object o, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        outputMessage.getHeaders().setContentType(APPLICATION_OCTET_STREAM);
        outputMessage.getHeaders().set("Content-Disposition", String.format("attachment; filename=%s", String.format("results_%s.txt", now().format(DATE_FORMAT))));
        StreamUtils.copy(o.toString(), getContentTypeCharset(outputMessage.getHeaders().getContentType()), outputMessage.getBody());

    }

    @Override
    protected Object readInternal(Class clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return null;
    }

    private Charset getContentTypeCharset(MediaType contentType) {
        if (contentType != null && contentType.getCharset() != null) {
            return contentType.getCharset();
        } else {
            return getDefaultCharset();
        }
    }

    @Override
    public Charset getDefaultCharset() {
        return defaultCharset();
    }
}
