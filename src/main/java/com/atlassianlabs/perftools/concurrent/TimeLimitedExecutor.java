package com.atlassianlabs.perftools.concurrent;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import com.google.common.util.concurrent.UncheckedTimeoutException;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.Executors.newCachedThreadPool;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Because we bind to JVM through sun.tools API either through reflection or a third-party library we can't 100% guarentee
 * that bind / collection attempts will timeout. This class exists as an executor for collection attempts that can be used
 * to ensure we will time-out collection attempts and terminate them, to avoid infinte thread execution.
 * <p>
 * Delegates to {@link TimeLimiter} with sensible defaults for consistent executions across the code-base.
 * Additionally holds a singleton of the {@link SimpleTimeLimiter}.
 */
@Component
public class TimeLimitedExecutor {
    private static final SimpleTimeLimiter TIME_LIMITER = new SimpleTimeLimiter();
    private static final ExecutorService EXECUTOR = newCachedThreadPool();

    private static final long DEFAULT_TIMEOUT = Integer.getInteger("collection.timeout", 5);

    /**
     * Delegates to {@link SimpleTimeLimiter#callWithTimeout(java.util.concurrent.Callable, long, java.util.concurrent.TimeUnit, boolean)} with
     * a timeout of 5s (unless overriden by the JVM argument {@literal -Dcollection.timeout}) and with interruptable true.
     *
     * @param callable The callable to execute.
     * @param <T>      Generic type of the passed callable.
     * @return The computed result of the callable if finished within 5s.
     * @throws Exception Any exceptions thrown during execution of callable.
     */
    public <T> T runSync(Callable<T> callable) throws Exception {
        try {
            return TIME_LIMITER.callWithTimeout(callable, DEFAULT_TIMEOUT, SECONDS, true);
        } catch (UncheckedTimeoutException e) {
            throw new TimeoutException(String.format("Could not finish within the timeout of %s seconds", DEFAULT_TIMEOUT));
        }
    }

    /**
     * Submits a call to {@link #runSync(Callable)} inside an executor service.
     *
     * @param callable The callable to execute.
     * @param <T>      Generic type of the passed callable.
     * @return A future of the callable.
     */
    public <T> Future<T> runAsync(Callable<T> callable) {
        return EXECUTOR.submit(() -> runSync(callable));
    }

    /**
     * @return the Singleton of the {@link SimpleTimeLimiter} held in this class.
     */
    public TimeLimiter getTimeLimiter() {
        return TIME_LIMITER;
    }
}
