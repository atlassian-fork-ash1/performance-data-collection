package com.atlassianlabs.perftools.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

import static java.lang.Runtime.getRuntime;

/**
 * Provides access to the operating system runtime interactions.
 */
@Component
public class RunTimeHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(RunTimeHelper.class);

    /**
     * Allows us to send commands to be executed.
     *
     * @param command command with arguments to be passed to a process
     * @return process reference, empty if a spawn was unsuccessful
     */
    public Optional<Process> spawnProcessSafely(String... command) {
        try {
            if (command.length > 0) {
                LOGGER.debug("Executing {}", (Object[]) command);
                return Optional.of(getRuntime().exec(command));
            }
        } catch (IOException e) {
            LOGGER.warn("Failed to spawn a process", e);
        }
        return Optional.empty();
    }
}
