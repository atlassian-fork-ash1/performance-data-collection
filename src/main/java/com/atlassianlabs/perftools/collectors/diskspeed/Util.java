package com.atlassianlabs.perftools.collectors.diskspeed;

import java.text.NumberFormat;

class Util {
    private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();

    Util() {
        throw new IllegalStateException("Utility class");
    }

    static String format(long number) {
        return NUMBER_FORMAT.format(number);
    }
}
