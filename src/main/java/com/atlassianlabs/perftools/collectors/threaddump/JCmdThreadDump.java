package com.atlassianlabs.perftools.collectors.threaddump;

import com.atlassianlabs.perftools.concurrent.TimeLimitedExecutor;
import com.atlassianlabs.perftools.virtualmachines.AutoCloseableHotSpotVm;
import com.atlassianlabs.perftools.virtualmachines.VirtualMachineFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.StringWriter;

import static java.util.Objects.requireNonNull;

@Component
public class JCmdThreadDump implements ThreadDumpCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(JCmdThreadDump.class);
    private static final String THREAD_PRINT = "Thread.print";
    private static final int PRIORITY = 0;

    private final VirtualMachineFactory virtualMachineFactory;
    private final TimeLimitedExecutor timeLimitedExecutor;

    @Autowired
    public JCmdThreadDump(@NotNull VirtualMachineFactory virtualMachineFactory, @NotNull final TimeLimitedExecutor timeLimitedExecutor) {
        this.virtualMachineFactory = requireNonNull(virtualMachineFactory);
        this.timeLimitedExecutor = requireNonNull(timeLimitedExecutor);
    }

    /**
     * Issues a thread.print using the same approach JCmd does to collect a thread dump.
     *
     * @param pid The Java process ID to take a thread dump of.
     * @return The data collected in String form.
     * @throws Exception if the results from jcmd cannot be read / copied or the VM cannot be connected to, or the executor fails
     */
    @Override
    public String collectFromProcess(long pid) throws Exception {
        return timeLimitedExecutor.runSync(() -> {
            final StringWriter out = new StringWriter();

            LOGGER.debug("Attempting to bind to pid " + pid);
            try (final AutoCloseableHotSpotVm vm = virtualMachineFactory.getHotSpotVirtualMachine(pid)) {
                LOGGER.debug("Attemping to execute JCmd " + THREAD_PRINT);

                IOUtils.copy(vm.executeJCmd(THREAD_PRINT), out);
                return out.toString();
            }
        });
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }
}
