package com.atlassianlabs.perftools.collectors.results;


import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import static com.google.common.collect.ImmutableList.copyOf;
import static java.util.stream.Collectors.joining;

/**
 * Contains a reference of multiple results so we provide a combination of all in one response.
 */
public class ComboResult {
    @ApiModelProperty(value = "A list of the combined cpu usage results")
    private final List<CpuUsageResult> cpuUsageResults;
    @ApiModelProperty(value = "A list of the combined thread dump results")
    private final List<ThreadDumpResult> threadDumpResults;

    public ComboResult(List<CpuUsageResult> cpuUsageResults, List<ThreadDumpResult> threadDumpResults) {
        this.cpuUsageResults = copyOf(cpuUsageResults);
        this.threadDumpResults = copyOf(threadDumpResults);
    }

    public List<CpuUsageResult> getCpuUsageResults() {
        return copyOf(cpuUsageResults);
    }

    public List<ThreadDumpResult> getThreadDumpResults() {
        return copyOf(threadDumpResults);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getCpuUsageResults().stream().map(CpuUsageResult::toString).collect(joining("\n")));
        sb.append("\n");
        sb.append(getThreadDumpResults().stream().map(ThreadDumpResult::toString).collect(joining("\n")));

        return sb.toString();
    }
}
