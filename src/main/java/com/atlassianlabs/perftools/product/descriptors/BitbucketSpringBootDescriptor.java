package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

// Bitbucket 5.0 uses spring boot, so it's different.
@Component
public class BitbucketSpringBootDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        return "com.atlassian.bitbucket.internal.launcher.BitbucketServerLauncher start";
    }

    @Override
    public String getInstProp() {
        return "bitbucket.install";
    }

    @Override
    public String getHomeProp() {
        return "bitbucket.home";
    }

    @Override
    public String getLogPath() {
        return "log";
    }

    @Override
    public String getName() {
        return "Bitbucket Server";
    }
}
