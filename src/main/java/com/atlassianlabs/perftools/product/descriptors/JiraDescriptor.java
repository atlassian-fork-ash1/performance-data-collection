package com.atlassianlabs.perftools.product.descriptors;

import org.springframework.stereotype.Component;

@Component
public class JiraDescriptor implements ProductDescriptor {

    @Override
    public String getDisplayName() {
        return "org.apache.catalina.startup.Bootstrap start";
    }

    @Override
    public String getInstProp() {
        return "catalina.base";
    }

    @Override
    public String getHomeProp() {
        return "jira.home";
    }

    @Override
    public String getHomeFile() {
        return "atlassian-jira/WEB-INF/classes/jira-application.properties";
    }

    @Override
    public String getLogPath() {
        return "log";
    }

    @Override
    public String getName() {
        return "JIRA";
    }
}
